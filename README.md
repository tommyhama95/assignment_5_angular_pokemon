# PokemonCatcher 🐲

Assignment for Angular during Noroff Accelerate program

## Description

This project is created with Angular and using a really big nested Pokemon API

## Heroku hosting

[@Michael](https://gitlab.com/kostmic) - https://damp-retreat-66799.herokuapp.com/

[@Tommy](https://gitlab.com/tommyhama95) - https://mighty-temple-54455.herokuapp.com/login

[@Alexander](https://gitlab.com/maaby200) - https://infinite-fjord-00717.herokuapp.com/login

## Sample data
> User interface
```js
interface {
  id: number,
  username: string,
  pokemon: Pokemon[]
}
```
> Pokemon interface
```js
interface Pokemon {
  id: number
  name: string;
  url: string;
  avatar: string;
}
```
## Folder Structure 
```
src/
    app/
        components/
            header/
            pokemoncard/
        guards/
        pages/
            catalogue/
            login/
            trainer/
        services/
    assets/
        fonts/
    environments/

```

## Component tree
<img src="./src/assets/cp-tree.png">

------

## Wireframes

### Login page
<img src="./src/assets/login.png">

### Trainer page
<img src="./src/assets/trainer.png">

### Catalogue page
<img src="./src/assets/catalogue.png">

## Additional comments
We decided to make it so that a user can only capture one of each Pokemon.

## Bugs
There are some bugs known in this solution

- A render bug occurres when searching for a pokemon in the Catalogue page. The top pagination part doesn't render until after the next value is set, even though they are the same.

- If you are fast as a bullet, you CAN capture MORE THAN ONE of a pokemon. When capturing a pokemon, spam the button before it becomes caught. Don't be greedy, share with others ;)

- Possible navigation routing bug with the prefix of setting all non-found routes to redirect to login. Both the below seem to work and sometimes not. Not sure excactly as of how and why.

```js
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '/login',
  }
// And
  {
    path: '(**)',
    pathMatch: 'full',
    redirectTo: '/login',
  }

```

## Not fixed or solved

- Conditional navigationbar rendering if a user is logged in or not.
- User being removed from Angulars state. Somehow it is still kept when logged out and you can still navigate to Trainer and Catalogue page.
- While logged in the user will still be able to access the login page, but upon clicking the "logout" button or visiting the /login url the local storage will be cleared and the user will have to login again

<img src="https://i.imgur.com/EQO71tX.jpg">
