import { Component } from "@angular/core";
import { UserSessionService } from "src/app/services/user-session.service";

@Component({
    selector: "app-trainer-page",
    templateUrl: "./trainer.page.html",
    styleUrls: ["./trainer.page.css"]
})
export class TrainerPage {

    constructor(private readonly userSession: UserSessionService) { }

    pokemons = this.userSession.user?.pokemon
    canOnlyCapture = false;
}

