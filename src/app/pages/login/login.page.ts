import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css'],
})
export class LoginPage {
  constructor(
    private readonly userService: UserService,
    private readonly router: Router
  ) {}

  public onClicked(username: string) {
    if (username != '') {
      this.userService.authenticate(username, async () => {
        await this.router.navigate(['trainer']);
      });
    }
  }
  public onEnter(username: string) {
    if (username != '') {
      this.userService.authenticate(username, async () => {
        await this.router.navigate(['trainer']);
      });
    }
  }
}
