import { Component , OnInit } from "@angular/core";
import { PokemonCatalogueService } from "../../services/pokemon-catalogue.service";
import { Pokemon } from "../../models/pokemon.model";
import { SessionService } from "../../services/pokemon-session.service";

@Component({
    selector: "app-catalogue-page",
    templateUrl: `catalogue.page.html`,
    styleUrls: ['catalogue.page.css']
})
export class CataloguePage implements OnInit{

  public pokeList:Pokemon[] | undefined;
  page: number = 0;
  amountOfPages: number = 0;
  pokemonPerPage: number = 20;
  pokemonPerPagesOptions: number[] = [20, 50, 100];
  pages: number[] = [];
  pokemonSearch: string = "";

  constructor(
    private readonly catalogueService: PokemonCatalogueService,
    private readonly pokeSession: SessionService) {
    this.pokeList = this.pokemonListDisplay("");
  }

  ngOnInit(): void {
    this.catalogueService.fetchPokemon()
    this.pokemonListPageFilter()
  }

  pokemonListDisplay(search : string):Pokemon[] | undefined {
    this.amountOfPages = this.pokeSession.pokeList?.length || 0;
    if(search.length < 1) {
      return this.pokeSession.pokeList?.slice(this.page * this.pokemonPerPage, (this.page + 1) * this.pokemonPerPage) || [];
    }
    else {
      this.amountOfPages = 1;
      this.pokemonListPageFilter();
      return this.pokeSession.pokeList?.filter(poke => poke.name.includes(search)).slice(this.page * this.pokemonPerPage, (this.page + 1) * this.pokemonPerPage) || [];
    }
  }

  //Fills the page array with as many elements needed based upon the calculated amount of pages and pokemon per page selected.
  pokemonListPageFilter():void {
    this.page = 0;
    let newPageArray: number[] = [];
    for(let i = 0; i < this.amountOfPages / this.pokemonPerPage; i++) {
      newPageArray.push(i);
    }
    this.pages = [...newPageArray];
  }

  onPageClicked(page : number) {
    this.page = page;
  }

  onKeyup(value: string) {
    this.pokemonSearch = value;
    if (value.length === 0) {
      this.amountOfPages = this.pokeSession.pokeList?.length || 0;
      this.pokemonListPageFilter();
    }
  }
}
