import { Component } from "@angular/core";

@Component({
    selector: "app-section",
    template: `
        <section class="pokemon-list">
            <ng-content></ng-content>
        </section>
    `,
    styles: [`
        .pokemon-list {
            margin: 0 auto;
            min-width: 80%;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            gap: 3rem;
            justify-content: center;
        }
    `]
})
export class SectionComponent {

}