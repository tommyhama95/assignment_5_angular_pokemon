import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserSessionService } from 'src/app/services/user-session.service';

@Component({
  selector: 'app-header',
  template: `
    <header class="header">
      <h1 class="title">PokeSite</h1>
      <nav class="navbar">
        <li class="nav-link"><a routerLink="/trainer">Trainer Box</a></li>
        <li class="nav-link"><a routerLink="/catalogue">Catalogue</a></li>
        <li class="nav-link">
          <a role="button" (click)="logoutClick()" routerLink="/catalogue"
            >Logout</a
          >
        </li>
      </nav>
    </header>
  `,
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent {
  constructor(
    private readonly router: Router,
    private readonly usersession: UserSessionService
  ) {}
  logoutClick() {
    this.usersession.removeUser();
    this.router.navigate(['login']);
    alert('You have been logged out');
  }
}
