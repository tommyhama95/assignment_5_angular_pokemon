import { Component, Input } from "@angular/core";
import { Observable } from "rxjs";
import { switchMap } from "rxjs/operators";
import { UserService } from "src/app/services/user.service";
import { Pokemon } from "../../models/pokemon.model";
import { UserSessionService } from "../../services/user-session.service";

@Component({
    selector: "app-pokemon-card",
    template: `
        <article class="card" *ngIf="pokemon">
            <img class="image" src={{pokemon.avatar}} (error)="onImageError($event)" />
            <p class="pokemon-name">{{pokemon.name}}</p>
            <div class="catchholder" *ngIf="notCaught; else caught">
                <label for={{pokemon.name}}>Catch</label>
                <input (click)="catchPokemon($event)" class="greyed" type="image" id={{pokemon.id}} alt="catch" src={{ballImage}}/>
            </div>
            <ng-template #caught>
                <div class="catchholder">
                    <label>Caught</label>
                    <input (click)="releasePokemon($event)" type="image" id={{pokemon.id}} src={{ballImage}} class="ball" [attr.disabled]="canOnlyCapture"/>
                </div>
            </ng-template>
        </article>
    `,
    styleUrls: ["./pokemoncard.component.css"]
})
export class PokemonCardComponent {

    @Input() pokemon?: Pokemon;
    @Input() canOnlyCapture?: boolean | null;
    notCaught?: boolean = true;
    ballImage = "../../../assets/pokeball.png";

    constructor(
        private readonly userSession: UserSessionService,
        private readonly userService: UserService
    ) { }


    ngOnInit() {
        this.userService.refreshUsersPokemonList
        .subscribe(() => {
            this.checkIfCaught();
        })

        this.checkIfCaught();
    }

    checkIfCaught() {
        if(this.pokemon !== undefined) {
            for(let userPokemon of this.userSession.user!.pokemon) {
                if(this.pokemon.name === userPokemon.name) {
                    this.notCaught = false;
                }
            }
        }
    }

    catchPokemon(event: any) {
        this.userSession.addPokemon(event.target.id)
        this.userService.updateUser(this.userSession.user!, () => {})
    }

    releasePokemon(event: any) {
        const wantToRelease = confirm(`Do you want to release ${this.pokemon!.name}?`)
        if(wantToRelease) {
            this.userSession.removePokemon(event.target.id)
            this.userService.updateUser(this.userSession.user!, () => {})
        }
    }

    onImageError(event: any) {
        event.target.src = "../../../assets/missingno.png"
    }
}

