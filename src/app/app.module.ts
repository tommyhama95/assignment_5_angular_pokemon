import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRouterModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { PokemonCardComponent } from './components/pokemoncard/pokemoncard.component';
import { SectionComponent } from './components/section.component';
import { LoginGuard } from './guards/login.guard';
import { CataloguePage } from './pages/catalogue/catalogue.page';
import { LoginPage } from './pages/login/login.page';
import { TrainerPage } from './pages/trainer/trainer.page';

@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    HeaderComponent,
    TrainerPage,
    CataloguePage,
    PokemonCardComponent,
    SectionComponent,
  ],
  imports: [BrowserModule, AppRouterModule, HttpClientModule, FormsModule],
  providers: [LoginGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}
