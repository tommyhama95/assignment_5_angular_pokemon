import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { SessionService } from './pokemon-session.service';

@Injectable({
  providedIn: 'root',
})
export class UserSessionService {
  private KEY = 'user';
  private _user: User | undefined;

  constructor(private readonly pokeSession: SessionService) {
    const storedUser = localStorage.getItem(this.KEY);
    if (storedUser) {
      this._user = JSON.parse(storedUser) as User;
    }
  }

  get user(): User | undefined {
    return this._user;
  }

  setUser(user: User): void {
    this._user = user;
    localStorage.setItem(this.KEY, JSON.stringify(user));
  }

  refreshUser(user: User): void {
    localStorage.removeItem(this.KEY);
    this.setUser(user);
  }

  removeUser() {
    localStorage.removeItem(this.KEY);
    this._user = undefined;
  }

  addPokemon(id: string) {
    const newPokemon: Pokemon = this.pokeSession.pokeList![parseInt(id)];
    this._user?.pokemon.push(newPokemon);
    this.refreshUser(this._user!);
  }

  removePokemon(id: string) {
    const pkmn: Pokemon[] | undefined = this._user?.pokemon.filter(
      (pkmn) => pkmn.id === parseInt(id)
    );
    if (pkmn) {
      const index = this._user!.pokemon.indexOf(pkmn[0]);
      this._user?.pokemon.splice(index, 1);
      this.refreshUser(this._user!);
    }
  }
}
