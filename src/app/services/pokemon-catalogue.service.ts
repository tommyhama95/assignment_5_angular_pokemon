import { Injectable } from '@angular/core';
import {Pokemon} from "../models/pokemon.model";
import {HttpClient} from "@angular/common/http";
import {SessionService} from "./pokemon-session.service";

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {

  public pokemon: Pokemon[] = [];
  public pokemonList: Pokemon[] = [];
  public POKEMON_URL: string = 'https://pokeapi.co/api/v2/pokemon?offset=0&limit=2000';

  constructor(
    private readonly http:HttpClient,
    private readonly pokesession:SessionService) {
  }
  //Entry point for populating the app with Pokemon Array either from session storage or PokeAPI
  public fetchPokemon(): void {
    if(this.pokesession.pokeList == undefined) {
      this.http.get<Pokemon[]>(this.POKEMON_URL).subscribe((response: any) => {
        this.pokemon = response.results
        this.populatePokemon();
      })
    }
  }
  public populatePokemon(): void {
    let newPokemonList: Pokemon[] = []
    for(let i = 0; i < 1118; i++) {
      let newPokemon:Pokemon = {
        name: this.pokemon[i].name,
        id: i,
        avatar: this.getImageURL(this.sliceId(this.pokemon[i])),
        url: this.pokemon[i].url
      }
      newPokemonList.push(newPokemon)
    }
    this.pokemonList = [...newPokemonList];
    this.pokesession.setPokeList([...newPokemonList])
  }

  public sliceId(poke:Pokemon):string {
    return poke.url.split('/')[6]}

  private getImageURL(id:string):string {
    return ("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + id + ".png").toString()
  }
}
