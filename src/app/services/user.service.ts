import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of, Subject } from "rxjs";
import { finalize, retry, switchMap, tap } from "rxjs/operators";
import { environment } from "src/environments/environment.prod";
import { User } from "../models/user.model";
import { UserSessionService } from "./user-session.service";
const API_URL = environment.apiBaseUrl

@Injectable({
  providedIn: 'root'
})
export class UserService{

  public attempting: boolean = false
  public error: string = ""

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'X-API-KEY': 'lRpvB7uCj9W2rpG5VKfwojkIN09wTgnZR2FJCokM32yjb1Kc46avOtVe6lUASEYh'
    })
  };

  private _refreshUsersPokemonList = new Subject<void>();
  
  constructor (
    private readonly http: HttpClient,
    private readonly userSession: UserSessionService
    ){
  }

  get refreshUsersPokemonList() {
    return this._refreshUsersPokemonList;
  }

  private findByUsername(username: string): Observable<User[]>{
    return this.http.get<User[]>(`${API_URL}?username=${username}`)
  }

  private createUser(username: string): Observable<User>{
    return this.http.post<User>(`${API_URL}`, {username, pokemon: []}, this.httpOptions)
  }

  public authenticate(username: string, onSuccess: () => void): void{
    this.attempting = true
    this.findByUsername(username)
    .pipe(
      retry(3),
      switchMap((users: User[]) => {
        if(users.length > 0){
          return of(users[0])
        }
        return this.createUser(username)
      } ),
      tap((user: User) => { //Side effects unchanged reponse

      }),
      finalize(() => {
        this.attempting = false
      })
    )
    .subscribe(
      (user: User) => { //Success
        if(user.id){
          onSuccess()
        this.userSession.setUser(user); 
        }
      },
      (error: HttpErrorResponse) => { //Error
        this.error = error.message
      }
    )
  }
  private updateUserPokemon(user: User): Observable<User> {
    return this.http.patch<User>(
      `${API_URL}/${user.id}`,
      user,
      this.httpOptions
    );
  }
  public updateUser(user: User, onSuccess: () => void): void {
    this.updateUserPokemon(user)
    .pipe(
      tap(() => {
        this._refreshUsersPokemonList.next();
      })
    )
    .subscribe(
      (user: User) => {
        //Success
        if (user.id) {
          onSuccess();
        }
      },
      (error: HttpErrorResponse) => {
        //Error
        this.error = error.message;
      }
    );
  }
}
