import { Injectable } from "@angular/core";
import {Pokemon} from "../models/pokemon.model";

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private _pokemonList: Pokemon[] | undefined;

  constructor() {
    const storedPokelist = localStorage.getItem('pokemonlist')
    if (storedPokelist) {
      this._pokemonList = JSON.parse(storedPokelist) as Pokemon[];
    }
  }

  get pokeList(): Pokemon[] | undefined {
    return this._pokemonList;
  }

  setPokeList(pokelist:Pokemon[]): void {
    this._pokemonList = pokelist;
    localStorage.setItem('pokemonlist', JSON.stringify(pokelist))
  }

}
